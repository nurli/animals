package com.example.renofinsa.animals.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renofinsa.animals.R;

import java.util.ArrayList;
import java.util.Locale;

public class Kuda extends AppCompatActivity {

    public static final int REQ_CODE_SPEECH_INPUT = 9000;
    ImageView back, ivKuda;
    Button speak;
    MediaPlayer player;
    TextView lebel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuda);

        back = findViewById(R.id.ivBack);
        speak = findViewById(R.id.btnSpeakKuda);
        lebel = findViewById(R.id.tvLebel);
        ivKuda = findViewById(R.id.ivKuda);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        speak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, Locale.getDefault());

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say somthing?");

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        }catch (ActivityNotFoundException a){
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support speech input",
                    Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    //labelView.setText(result.get(0));
                    setResult(result.get(0));

                }
                break;
            }

        }
    }

    private void setResult(String result) {
        lebel.setText(result);
        result = result.toLowerCase();

        if ( "horse".startsWith(result)) {
            player = new MediaPlayer();
            play("kuda.wav");
            ivKuda.setImageResource(R.drawable.kuda);
        }
    }

    private void play(String filename) {
        try {
            AssetFileDescriptor assetFileDescriptor = this.getAssets().openFd(filename);
            player.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            assetFileDescriptor.close();
            player.prepare();
            player.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
