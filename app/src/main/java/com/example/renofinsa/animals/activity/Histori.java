package com.example.renofinsa.animals.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.renofinsa.animals.R;
import com.example.renofinsa.animals.adapter.CustomAdapter;

import java.util.ArrayList;

public class Histori extends AppCompatActivity {

    private ListView listView;
    private CustomAdapter customAdapter;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    Context context;
    ImageView imageView;

    String[] namaHewan = {
            "Elephant",
            "Bird"
    };

    String[] score ={
            "100",
            "0",
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histori);

        //listView = findViewById(R.id.lv);
        recyclerView = findViewById(R.id.rvHistory);
        imageView = findViewById(R.id.ivBack);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        customAdapter = new CustomAdapter(this, namaHewan, score);
        recyclerView.setAdapter(customAdapter);



//        customAdapter = new CustomAdapter(this, namaHewan,score );
//        listView.setAdapter(customAdapter);

    }
}
