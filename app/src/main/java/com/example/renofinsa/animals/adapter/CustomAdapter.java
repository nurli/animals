package com.example.renofinsa.animals.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.renofinsa.animals.R;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ModulHolder> {

    Context context;
    String[] namaHewan;
    String[] score;

    public CustomAdapter(Context context, String[] namaHewan, String[] score) {
        this.context = context;
        this.namaHewan = namaHewan;
        this.score = score;
    }

    @NonNull
    @Override
    public ModulHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_history, viewGroup, false);
        return new ModulHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ModulHolder modulHolder, int i) {
        modulHolder.nama.setText(namaHewan[i]);
        modulHolder.score.setText(score[i]);
    }

    @Override
    public int getItemCount() {
        return namaHewan.length;
    }

    class ModulHolder extends RecyclerView.ViewHolder{
        TextView nama, score;
        public ModulHolder(@NonNull View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.tvNamaHewan);
            score = itemView.findViewById(R.id.tvValue);
        }
    }

}

