package com.example.renofinsa.animals.activity;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.MainThread;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.renofinsa.animals.R;

public class Suara extends AppCompatActivity {

    Button btnOn, btnOff;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suara);

        init();

        final AudioManager manager =  (AudioManager) getSystemService(
                getApplicationContext().AUDIO_SERVICE
        );


        btnOff.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.O_MR1)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                int previous_notification_interrupt_setting = notificationManager.getCurrentInterruptionFilter();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                        && !notificationManager.isNotificationPolicyAccessGranted()) {
                    manager.setRingerMode(AudioManager.RINGER_MODE_SILENT);

                }else {
                    Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                    startActivity(intent);
                }

// if (notificationManager.isNotificationPolicyAccessGranted()){
//                    manager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//                }else {
//                    Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
//                    startActivity(intent);
//                }
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    previous_notification_interrupt_setting = notificationManager.getCurrentInterruptionFilter();
//                    manager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//                    notificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_NONE);
//
//                }


            }
        });
        btnOn.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
//                manager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                if (notificationManager.isNotificationPolicyAccessGranted()){
                    manager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                }else {
                    Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                    startActivity(intent);
                }
            }
        });
    }

    private void init() {
        btnOn = findViewById(R.id.btnOn);
        btnOff = findViewById(R.id.btnOff);
    }

}
