package com.example.renofinsa.animals.activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renofinsa.animals.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Locale;

public class Gajah extends AppCompatActivity {

    public static final int REQ_CODE_SPEECH_INPUT = 9000;
    ImageView back, ivGajah, score, failed;
    Button speak;
    MediaPlayer player;
    TextView lebel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gajah);

        back = findViewById(R.id.ivBack);
        speak = findViewById(R.id.btnSpeakGajah);
        lebel = findViewById(R.id.tvLebel);
        ivGajah = findViewById(R.id.ivGajah);
        score = findViewById(R.id.ivScore);
        failed = findViewById(R.id.ivFailed);

        score.setVisibility(View.GONE);
        failed.setVisibility(View.GONE);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                simpanNilai();
            }
        });
        speak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });
    }

    private void simpanNilai() {
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, Locale.getDefault());

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say somthing?");

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        }catch (ActivityNotFoundException a){
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support speech input",
                    Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    //labelView.setText(result.get(0));
                    setResult(result.get(0));

                }
                break;
            }

        }
    }

    @SuppressLint("ResourceAsColor")
    private void setResult(String result) {
        lebel.setText(result);
        result = result.toLowerCase();

        if ( "elephant".startsWith(result)) {
            player = new MediaPlayer();
            play("gajah.wav");
            score.setVisibility(View.VISIBLE);
            failed.setVisibility(View.GONE);
            Toast.makeText(this, "Congratulations you are right", Toast.LENGTH_SHORT).show();
        }else {
            player = new MediaPlayer();
            play("fail-trumpet.wav");
            score.setVisibility(View.GONE);
            failed.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Ups, You Failed", Toast.LENGTH_SHORT).show();
//            ivGajah.setImageResource(R.drawable.ic_fail);
//            lebel.setTextColor(R.color.colorRed);
//            lebel.setText("You are wrong");
//            play("fail-trumpet.wav");
//            onBackPressed();
        }
    }

    private void play(String filename) {
        try {
            AssetFileDescriptor assetFileDescriptor = this.getAssets().openFd(filename);
            player.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            assetFileDescriptor.close();
            player.prepare();
            player.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
