package com.example.renofinsa.animals.activity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renofinsa.animals.R;

public class PengaturanSuara extends AppCompatActivity {

    RadioButton hidup, mati;
    ImageView sound, addVol, minVol, back;
    SeekBar seekBar;
    AudioManager audioManager;
    Button btnHidup, btnMati;
    RadioGroup radioGroup;
    TextView tvNumberVol;
    Switch onOff;
    String sSwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan_suara);

//        hidup = findViewById(R.id.rbHidup);
//        mati = findViewById(R.id.rbMati);
        sound = findViewById(R.id.ivSound);
        addVol = findViewById(R.id.tambahVol);
        minVol = findViewById(R.id.kurangVol);
        seekBar = findViewById(R.id.sbVolume);
//        radioGroup = findViewById(R.id.rgVol);
        back = findViewById(R.id.ivBack);
        tvNumberVol = findViewById(R.id.tvNumberVol);
        btnHidup = findViewById(R.id.btnSuaraHidup);
        btnMati = findViewById(R.id.btnSuaraMati);
        onOff = findViewById(R.id.switchVol);


        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20,0);

        if (onOff.isChecked()){
//            onOff.setTextOn("On");
//            sSwitch = onOff.getTextOn().toString();
            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER,
                    AudioManager.VIBRATE_SETTING_ON);
        }else {
//            onOff.setTextOff("Off");
//            sSwitch = onOff.getTextOff().toString();
            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
            audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER,
                    AudioManager.VIBRATE_SETTING_OFF);
        }



//        initBar(seekBar, AudioManager.STREAM_RING);

//        initControls();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//        setAudioMode();
//        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20,0);

//        btnHidup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
//                        AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
////                audioManager.setSpeakerphoneOn(true);
////                audioManager.getStreamVolume(AudioManager.RINGER_MODE_NORMAL);
////                audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
//                Toast.makeText(PengaturanSuara.this, "Now is Ringging Mode", Toast.LENGTH_SHORT).show();
//                sound.setImageResource(R.drawable.vol);
//
//            }
//        });
//        btnMati.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
//                        AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
////                audioManager.setSpeakerphoneOn(false);
////                audioManager.getStreamVolume(AudioManager.RINGER_MODE_SILENT);
////                audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//                Toast.makeText(PengaturanSuara.this, "Now is Vibrate Mode", Toast.LENGTH_SHORT).show();
//                sound.setImageResource(R.drawable.sound_off);
//
//            }
//        });

        seekBar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        seekBar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

        addVol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + 1);
            }
        });
        minVol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) - 1);
            }
        });



        seekBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        tvNumberVol.setText("Media Volume : " + progress);
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
//        radioGroup.setOnCheckedChangeListener(
//                new RadioGroup.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(RadioGroup group, int checkedId) {
//                        int index = group.indexOfChild(findViewById(group.getCheckedRadioButtonId()));
//                        System.out.println("Selected Button id "+ index);
//                        if (checkedId == R.id.rbHidup){
//                            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
//                            hidup.setChecked(true);
//                            sound.setImageResource(R.drawable.vol);
//                        }
//                        if (checkedId == R.id.rbMati){
//                            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//                            mati.setChecked(true);
//                            sound.setImageResource(R.drawable.sound_off);
//                        }
//                    }
//                }
//        );
    }

//    private void setAudioMode() {
//        System.out.println("Current Ringing Mode "+ audioManager.getMode());
//        if (audioManager.getRingerMode() == 0){
//            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
//            hidup.setChecked(true);
//        }
//        if (audioManager.getRingerMode() == 1){
//            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//            mati.setChecked(true);
//        }
//    }


//    private void initBar(SeekBar bar, final int stream) {
//        bar.setMax(audioManager.getStreamMaxVolume(stream));
//        bar.setProgress(audioManager.getStreamVolume(stream));
//
//        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            public void onProgressChanged(SeekBar bar, int progress,
//                                          boolean fromUser) {
//                audioManager.setStreamVolume(stream, progress,
//                        AudioManager.FLAG_PLAY_SOUND);
//            }
//
//            public void onStartTrackingTouch(SeekBar bar) {
//                // no-op
//            }
//
//            public void onStopTrackingTouch(SeekBar bar) {
//                // no-op
//            }
//        });
//    }

    private void initControls() {
        try {
            seekBar = findViewById(R.id.sbVolume);
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            seekBar.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            seekBar.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));


            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        final  AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
//            try {
//                seekBar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) - 1);
//            }catch (Error e){
//
//            }
//        }else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP){
//            try {
//                seekBar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + 1);
//            }catch (Error e){
//
//            }
//
//        }
//        return super.onKeyDown(keyCode, event);
//    }
}
