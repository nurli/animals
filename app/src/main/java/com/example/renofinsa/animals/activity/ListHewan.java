package com.example.renofinsa.animals.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.renofinsa.animals.R;

public class ListHewan extends AppCompatActivity {

    ImageView back;
    Button gajah, burung, monyet, katak, kuda, lumba, sapi, serigala, singa, lebah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_hewan);

        back = findViewById(R.id.ivBack);
        gajah = findViewById(R.id.btnGajah);
        burung = findViewById(R.id.btnBurung);
        monyet = findViewById(R.id.btnMonyet);
        katak = findViewById(R.id.btnKatak);
        kuda = findViewById(R.id.btnKuda);
        lumba = findViewById(R.id.btnLumba);
        sapi = findViewById(R.id.btnSapi);
        serigala = findViewById(R.id.btnSerigala);
        singa = findViewById(R.id.btnSinga);
        lebah = findViewById(R.id.btnLebah);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        gajah.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Gajah.class));
//                gajah.setEnabled(false);
//                gajah.setTextColor(R.color.colorGrey);
            }
        });
        burung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Burung.class));

            }
        });
        monyet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Keledai.class));

            }
        });
        katak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Katak.class));

            }
        });
        kuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Kuda.class));

            }
        });
        lebah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Lebah.class));

            }
        });
        lumba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Lumba.class));

            }
        });
        sapi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Sapi.class));

            }
        });
        serigala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Serigala.class));

            }
        });
        singa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListHewan.this, Singa.class));

            }
        });



    }
}
