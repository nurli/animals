package com.example.renofinsa.animals.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.renofinsa.animals.R;

public class Start extends AppCompatActivity {


    Button namaHewan, pengaturan, keluar, history;
    ImageView informasi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        namaHewan = findViewById(R.id.btnNamaHewan);
        pengaturan = findViewById(R.id.btnPengaturan);
        keluar = findViewById(R.id.btnKeluar);
        history = findViewById(R.id.btnHistory);
        informasi = findViewById(R.id.ivInfo);

        informasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Start.this, Informasi.class));
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Start.this, Histori.class));

            }
        });
        namaHewan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Start.this, ListHewan.class));

            }
        });
        pengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Start.this, PengaturanSuara.class));

            }
        });
        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
//                keluarAplikasi();
            }
        });
    }

    private void keluarAplikasi() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Anda Yakin Ingin Log Out ?");
        alert.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        finish();
                    }
                });
        alert.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }
}
